{
  "bboxMode": 1,
  "collisionKind": 0,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 11,
  "bbox_top": 0,
  "bbox_bottom": 11,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 12,
  "height": 12,
  "textureGroupId": {
    "name": "MainMenu",
    "path": "texturegroups/MainMenu",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"e1720e24-c3c3-44d4-aea8-47fc9a070ca8","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e1720e24-c3c3-44d4-aea8-47fc9a070ca8","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"e1720e24-c3c3-44d4-aea8-47fc9a070ca8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"51f7d474-b08d-4769-9366-de07bf32337f","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"51f7d474-b08d-4769-9366-de07bf32337f","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"51f7d474-b08d-4769-9366-de07bf32337f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"eec98bd7-5770-4902-b74a-dd9c29d769db","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"eec98bd7-5770-4902-b74a-dd9c29d769db","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"eec98bd7-5770-4902-b74a-dd9c29d769db","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"95de3f34-74bd-4414-a281-ad52c890c803","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"95de3f34-74bd-4414-a281-ad52c890c803","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"95de3f34-74bd-4414-a281-ad52c890c803","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"17bcbcac-16f5-4525-9e5c-42f8b0fec525","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"17bcbcac-16f5-4525-9e5c-42f8b0fec525","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"17bcbcac-16f5-4525-9e5c-42f8b0fec525","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bf0b1247-c13d-45d4-abeb-49047f77f24a","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bf0b1247-c13d-45d4-abeb-49047f77f24a","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"bf0b1247-c13d-45d4-abeb-49047f77f24a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a1a4c8b2-c61d-4945-a116-9b98015f3079","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a1a4c8b2-c61d-4945-a116-9b98015f3079","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"a1a4c8b2-c61d-4945-a116-9b98015f3079","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"63498f01-2f4e-4d55-812b-e5a42aec2f72","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"63498f01-2f4e-4d55-812b-e5a42aec2f72","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"63498f01-2f4e-4d55-812b-e5a42aec2f72","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ea6cdd05-1031-49dc-935a-5f8e85c6697a","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ea6cdd05-1031-49dc-935a-5f8e85c6697a","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"ea6cdd05-1031-49dc-935a-5f8e85c6697a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a8403184-428e-4253-a4d5-67efd98cc3ed","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a8403184-428e-4253-a4d5-67efd98cc3ed","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"a8403184-428e-4253-a4d5-67efd98cc3ed","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a5631a53-7354-46e1-8a6f-501cf89abd3c","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a5631a53-7354-46e1-8a6f-501cf89abd3c","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"a5631a53-7354-46e1-8a6f-501cf89abd3c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"eb1ed101-8893-4a7c-b6ef-6506f816e4b3","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"eb1ed101-8893-4a7c-b6ef-6506f816e4b3","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"eb1ed101-8893-4a7c-b6ef-6506f816e4b3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"97379b16-9e98-462f-9e34-a4793c0e17fa","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"97379b16-9e98-462f-9e34-a4793c0e17fa","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"97379b16-9e98-462f-9e34-a4793c0e17fa","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"167e5958-918b-411d-a3a8-5350432ff8a6","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"167e5958-918b-411d-a3a8-5350432ff8a6","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"167e5958-918b-411d-a3a8-5350432ff8a6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4051f4fe-9aa1-4fc3-9e7a-9f6c23f41dba","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4051f4fe-9aa1-4fc3-9e7a-9f6c23f41dba","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"4051f4fe-9aa1-4fc3-9e7a-9f6c23f41dba","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"735b407e-e11d-4a1a-95b7-5d9e3d00aa44","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"735b407e-e11d-4a1a-95b7-5d9e3d00aa44","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"735b407e-e11d-4a1a-95b7-5d9e3d00aa44","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b1a26190-2cbb-40eb-b065-96fd4391bf16","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b1a26190-2cbb-40eb-b065-96fd4391bf16","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"b1a26190-2cbb-40eb-b065-96fd4391bf16","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e10e966b-803f-42a7-a9b8-e753237423aa","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e10e966b-803f-42a7-a9b8-e753237423aa","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"e10e966b-803f-42a7-a9b8-e753237423aa","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ff5065f8-e723-4a4f-890c-e78ceb6a8021","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ff5065f8-e723-4a4f-890c-e78ceb6a8021","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"ff5065f8-e723-4a4f-890c-e78ceb6a8021","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0380f493-e5a1-4e6b-a821-8c46e0436e7f","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0380f493-e5a1-4e6b-a821-8c46e0436e7f","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"0380f493-e5a1-4e6b-a821-8c46e0436e7f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"adab0fc8-b531-4c3b-8aac-8f3670de6bb6","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"adab0fc8-b531-4c3b-8aac-8f3670de6bb6","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"adab0fc8-b531-4c3b-8aac-8f3670de6bb6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"167e671e-2858-4473-b51d-43eb0819ffcf","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"167e671e-2858-4473-b51d-43eb0819ffcf","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"167e671e-2858-4473-b51d-43eb0819ffcf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"586b81e1-3ad9-46e1-82cf-e4a366fcbd77","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"586b81e1-3ad9-46e1-82cf-e4a366fcbd77","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"586b81e1-3ad9-46e1-82cf-e4a366fcbd77","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6e7ce0dd-7f14-4ecc-94cd-9f3fabaff9cb","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6e7ce0dd-7f14-4ecc-94cd-9f3fabaff9cb","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"6e7ce0dd-7f14-4ecc-94cd-9f3fabaff9cb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c110ed30-16f5-4496-af52-7b75a0bbcffd","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c110ed30-16f5-4496-af52-7b75a0bbcffd","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"c110ed30-16f5-4496-af52-7b75a0bbcffd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0ac3cda2-ba4d-4c75-9f7e-6f2b86da46ac","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0ac3cda2-ba4d-4c75-9f7e-6f2b86da46ac","path":"sprites/sprModHUD/sprModHUD.yy",},"LayerId":{"name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","name":"0ac3cda2-ba4d-4c75-9f7e-6f2b86da46ac","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 26.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"50311ae3-1032-4d50-af0b-e4651356e8e5","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e1720e24-c3c3-44d4-aea8-47fc9a070ca8","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a00e7674-38b6-4cb3-a4c1-c09d48e0a195","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"51f7d474-b08d-4769-9366-de07bf32337f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4021e1ba-ea57-4057-9086-7345a3d44022","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"eec98bd7-5770-4902-b74a-dd9c29d769db","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1dbf83d1-7f83-4d5a-aeeb-a394a5496f82","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"95de3f34-74bd-4414-a281-ad52c890c803","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"733c8c39-596b-4917-8eb9-7a95375f1300","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"17bcbcac-16f5-4525-9e5c-42f8b0fec525","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d493b520-6843-4e3f-90b6-b221054b6320","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bf0b1247-c13d-45d4-abeb-49047f77f24a","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b5550aea-6a8d-4179-b6bb-5b05a1d6a68b","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a1a4c8b2-c61d-4945-a116-9b98015f3079","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"39d4cc74-562a-4dbd-9c39-6fc69d09d663","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"63498f01-2f4e-4d55-812b-e5a42aec2f72","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5185e705-b4ed-4445-b075-f6d0726598c7","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ea6cdd05-1031-49dc-935a-5f8e85c6697a","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dcc63336-9b49-4807-9efc-0d58c6e553b8","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a8403184-428e-4253-a4d5-67efd98cc3ed","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4fa1404c-bf27-4709-87b7-b8d0bd6069ac","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a5631a53-7354-46e1-8a6f-501cf89abd3c","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3ca90f34-7b9e-4b23-b159-e803b7344f06","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"eb1ed101-8893-4a7c-b6ef-6506f816e4b3","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"337aca0f-b05a-4f57-b445-9dac8a0bc63a","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"97379b16-9e98-462f-9e34-a4793c0e17fa","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cc18961d-3073-4674-b587-b35dc1258da0","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"167e5958-918b-411d-a3a8-5350432ff8a6","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a68bdd81-d9c5-4065-b179-0eae759992b9","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4051f4fe-9aa1-4fc3-9e7a-9f6c23f41dba","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d3a41e9c-fe10-4302-b572-a200373d0cf3","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"735b407e-e11d-4a1a-95b7-5d9e3d00aa44","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a6a9ee7e-c0ee-4a88-9ffc-5b734b44dc32","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b1a26190-2cbb-40eb-b065-96fd4391bf16","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0d1605ed-a3e5-4aae-84ae-2a943e0a019a","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e10e966b-803f-42a7-a9b8-e753237423aa","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3e1ec833-5f7c-4e42-98da-d22379274797","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ff5065f8-e723-4a4f-890c-e78ceb6a8021","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"73593d0a-5f51-4821-ada5-e94a33744715","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0380f493-e5a1-4e6b-a821-8c46e0436e7f","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d3c774a9-e445-43b6-9eff-84913031f2bc","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"adab0fc8-b531-4c3b-8aac-8f3670de6bb6","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fb4701de-f8d2-4747-85a4-cef99d4f12e5","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"167e671e-2858-4473-b51d-43eb0819ffcf","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d16a9601-3ab2-4ea1-9ac6-8ed8ed8aad73","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"586b81e1-3ad9-46e1-82cf-e4a366fcbd77","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2afef8bb-72fb-4f5b-ae04-9fe058d32c0f","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6e7ce0dd-7f14-4ecc-94cd-9f3fabaff9cb","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ca3bcbcc-a6b1-4b90-b196-f100961b61f0","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c110ed30-16f5-4496-af52-7b75a0bbcffd","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"da4b6c7b-64a6-4c1a-ad93-e829e17e609c","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0ac3cda2-ba4d-4c75-9f7e-6f2b86da46ac","path":"sprites/sprModHUD/sprModHUD.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sprModHUD","path":"sprites/sprModHUD/sprModHUD.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"1789c11f-c8d2-4cf2-a3c3-46c9f6aced1f","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Menu",
    "path": "folders/Sprites/Menu.yy",
  },
  "resourceVersion": "1.0",
  "name": "sprModHUD",
  "tags": [],
  "resourceType": "GMSprite",
}