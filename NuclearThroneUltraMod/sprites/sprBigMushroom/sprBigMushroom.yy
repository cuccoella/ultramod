{
  "bboxMode": 1,
  "collisionKind": 0,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 29,
  "bbox_top": 0,
  "bbox_bottom": 28,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 30,
  "height": 29,
  "textureGroupId": {
    "name": "Wonderland",
    "path": "texturegroups/Wonderland",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"74d0bf1d-ac07-417e-b9c0-aa8def793d02","path":"sprites/sprBigMushroom/sprBigMushroom.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"74d0bf1d-ac07-417e-b9c0-aa8def793d02","path":"sprites/sprBigMushroom/sprBigMushroom.yy",},"LayerId":{"name":"591bae97-5627-493b-ba2c-455ec250d1d5","path":"sprites/sprBigMushroom/sprBigMushroom.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprBigMushroom","path":"sprites/sprBigMushroom/sprBigMushroom.yy",},"resourceVersion":"1.0","name":"74d0bf1d-ac07-417e-b9c0-aa8def793d02","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sprBigMushroom","path":"sprites/sprBigMushroom/sprBigMushroom.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"1ef7f75d-aa8b-4807-8048-9b04df3b1974","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"74d0bf1d-ac07-417e-b9c0-aa8def793d02","path":"sprites/sprBigMushroom/sprBigMushroom.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 14,
    "yorigin": 15,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sprBigMushroom","path":"sprites/sprBigMushroom/sprBigMushroom.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"591bae97-5627-493b-ba2c-455ec250d1d5","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Props",
    "path": "folders/Sprites/Enviroment/Props.yy",
  },
  "resourceVersion": "1.0",
  "name": "sprBigMushroom",
  "tags": [],
  "resourceType": "GMSprite",
}