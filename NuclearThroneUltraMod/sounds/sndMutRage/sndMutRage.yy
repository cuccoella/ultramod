{
  "conversionMode": 0,
  "compression": 1,
  "volume": 1.0,
  "preload": true,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sndMutRage",
  "duration": 2.832041,
  "parent": {
    "name": "Mutations",
    "path": "folders/Sounds/Mutations.yy",
  },
  "resourceVersion": "1.0",
  "name": "sndMutRage",
  "tags": [],
  "resourceType": "GMSound",
}