{
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sndThunder",
  "duration": 0.0,
  "parent": {
    "name": "Energy",
    "path": "folders/Sounds/Weapons/Energy.yy",
  },
  "resourceVersion": "1.0",
  "name": "sndThunder",
  "tags": [],
  "resourceType": "GMSound",
}