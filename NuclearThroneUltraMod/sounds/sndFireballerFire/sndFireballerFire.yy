{
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sndFireballerFire.ogg",
  "duration": 0.730952,
  "parent": {
    "name": "Enemies",
    "path": "folders/Sounds/Enemies.yy",
  },
  "resourceVersion": "1.0",
  "name": "sndFireballerFire",
  "tags": [],
  "resourceType": "GMSound",
}