{
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": true,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sndBloodCannonEnd",
  "duration": 2.608031,
  "parent": {
    "name": "Explosive",
    "path": "folders/Sounds/Weapons/Explosive.yy",
  },
  "resourceVersion": "1.0",
  "name": "sndBloodCannonEnd",
  "tags": [],
  "resourceType": "GMSound",
}