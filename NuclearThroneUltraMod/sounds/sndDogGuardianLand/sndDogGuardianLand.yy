{
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sndDogGuardianLand.wav",
  "duration": 0.255031,
  "parent": {
    "name": "Palace",
    "path": "folders/Sounds/Palace.yy",
  },
  "resourceVersion": "1.0",
  "name": "sndDogGuardianLand",
  "tags": [],
  "resourceType": "GMSound",
}