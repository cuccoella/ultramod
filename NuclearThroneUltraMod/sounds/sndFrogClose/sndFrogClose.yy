{
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sndFrogClose",
  "duration": 0.313016,
  "parent": {
    "name": "Enemies",
    "path": "folders/Sounds/Enemies.yy",
  },
  "resourceVersion": "1.0",
  "name": "sndFrogClose",
  "tags": [],
  "resourceType": "GMSound",
}