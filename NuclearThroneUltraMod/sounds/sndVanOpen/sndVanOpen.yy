{
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": true,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sndVanOpen",
  "duration": 0.0,
  "parent": {
    "name": "Elite",
    "path": "folders/Sounds/IDPD sounds/Elite.yy",
  },
  "resourceVersion": "1.0",
  "name": "sndVanOpen",
  "tags": [],
  "resourceType": "GMSound",
}