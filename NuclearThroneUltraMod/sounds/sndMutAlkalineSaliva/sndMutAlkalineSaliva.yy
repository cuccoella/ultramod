{
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sndMutAlkalineSaliva.wav",
  "duration": 1.962562,
  "parent": {
    "name": "Mutations",
    "path": "folders/Sounds/Mutations.yy",
  },
  "resourceVersion": "1.0",
  "name": "sndMutAlkalineSaliva",
  "tags": [],
  "resourceType": "GMSound",
}