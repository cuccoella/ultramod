{
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": true,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sndSalamanderHurt",
  "duration": 0.0,
  "parent": {
    "name": "Salamander",
    "path": "folders/Sounds/Player/Salamander.yy",
  },
  "resourceVersion": "1.0",
  "name": "sndSalamanderHurt",
  "tags": [],
  "resourceType": "GMSound",
}