raddrop = 30
maxhealth = 40
meleedamage = 0
size = 2

spr_idle = sprGuardianActivate
spr_walk = sprGuardianIdle
spr_hurt = sprGuardianHurt
spr_dead = sprGuardianDead
spr_fire = sprGuardianFire

event_inherited()

friction = 1


snd_hurt = sndHitRock
snd_dead = sndScorpionDie
snd_mele = sndScorpionMelee

//behavior
walk = 0
alarm[1] = 25
gunangle = random(360)
candeflect = 0
canfire = 1


