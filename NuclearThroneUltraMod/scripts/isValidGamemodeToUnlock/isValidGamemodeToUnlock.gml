///isValidGamemodeToUnlock();
// /@description returns true if a gamemode is valid for unlocking stuff
function isValidGamemodeToUnlock(gm){
	return (gm == 0 || gm == 8 || gm == 12 || gm == 3 || gm == 4 || gm == 7 || gm == 19 || gm == 16
	|| gm == 15 || gm == 6 || gm == 20 || gm == 22);
}